#!/bin/sh
#
# usage: $0 out
#
set -eux

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

OUT="${1}"

NITTER_DIR="${PROGBASE}/nitter"
DESTDIR="${PROGBASE}/rootfs"
NIM_VERSION='1.6.2'
NIM_SHA256='9e0c616fe504402e29e43d44708a49416dcbfa295cee1883ce6e5bfb25c364f1'

apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get -y install gcc git libsass-dev wget xz-utils

git clone --depth=1 https://github.com/zedeus/nitter "${NITTER_DIR}"
cd "${NITTER_DIR}"
wget "https://nim-lang.org/download/nim-${NIM_VERSION}-linux_x64.tar.xz"
h=$(sha256sum "nim-${NIM_VERSION}-linux_x64.tar.xz" | awk '{print $1;}')
[ X"${h}" = X"${NIM_SHA256}" ]
xz -cd "nim-${NIM_VERSION}-linux_x64.tar.xz" | tar -f- -x
PATH="${NITTER_DIR}/nim-${NIM_VERSION}/bin:${PATH}"
export PATH
nimble build -y -d:release
strip -s ./nitter
nimble scss
nimble md

mkdir -p "${DESTDIR}/srv/nitter/public/"
cp "${NITTER_DIR}/nitter" "${DESTDIR}/srv/nitter/"
chmod 0555 "${DESTDIR}/srv/nitter/nitter"
cp "${NITTER_DIR}/nitter.example.conf" "${DESTDIR}/srv/nitter/nitter.conf"
chmod 0640 "${DESTDIR}/srv/nitter/nitter.conf"
cp -R "${NITTER_DIR}/public" "${DESTDIR}/srv/nitter/"
mkdir -p "${DESTDIR}/etc/systemd/system/"
cp "${PROGBASE}/nitter.service" "${DESTDIR}/etc/systemd/system/"
chmod 0444 "${DESTDIR}/etc/systemd/system/nitter.service"
git rev-parse --short HEAD >"${DESTDIR}/srv/nitter/version.txt"

tar -C "${DESTDIR}" -f- -c . | gzip -c9 >"${1}"
