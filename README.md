# nitter build

Binary build of `nitter` a privacy-friendly and RSS-supporting alternative
Twitter frontend.

https://gitlab.com/pgregoire-ci/nitter/-/jobs/artifacts/main/raw/nitter.tar.gz?job=build


## Installation

Either use the Ansible role provided in this repository or:

```
apt-get -y install redis-server
useradd -s/bin/false -r nitter
gzip -cd "nitter.tar.gz" | (cd / && tar -f- -xp)
cp ~/nitter.conf /srv/nitter/nitter.conf
chgrp nitter /srv/nitter/nitter.conf
systemctl daemon-reload
systemctl enable nitter
systemctl start nitter
```

Note that placing `nitter` behind a reverse proxy is recommended by
the project.


## References

https://github.com/zedeus/nitter/
